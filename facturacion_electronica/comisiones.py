from facturacion_electronica import clase_util as util

class Comisiones(object):

    def __init__(self,vals=False):
        if vals:
            util.set_from_keys(vals)

    @property
    def TipoMovim(self):
        if not hasattr(self,'_tipo_movim'):
            return False
        return self._tipo_movim

    @TipoMovim.setter
    def TipoMovim(self,val):
        self._tipo_movim = val


    @property
    def Glosa(self):
        if not hasattr(self,'_glosa'):
            return False
        return self._glosa

    @Glosa.setter
    def Glosa(self,val):
        self._glosa = val

    @property
    def TasaComision(self):
        if not hasattr(self,'_tasa_comision'):
            return False
        return self._tasa_comision

    @TasaComision.setter
    def TasaComision(self,val):
        self._tasa_comision = val

    @property
    def ValComNeto(self):
        if not hasattr(self,'_val_com_neto'):
            return 0
        return self._val_com_neto

    @ValComNeto.setter
    def ValComNeto(self,val):
        self._val_com_neto = val

    @property
    def ValComExe(self):
        if not hasattr(self,'_val_com_exe'):
            return 0
        return self._val_com_exe

    @ValComExe.setter
    def ValComExe(self,val):
        self._val_com_exe = val

    @property
    def ValComIVA(self):
        if not hasattr(self,'_val_com_iva'):
            return 0
        return self._val_com_iva

    @ValComIVA.setter
    def ValComIVA(self,val):
        self._val_com_iva = val

