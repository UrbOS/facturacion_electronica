# -*- coding: utf-8 -*-
from lxml import etree
import unittest
import logging
from io import open
import json
from facturacion_electronica import facturacion_electronica as fe
import base64
_logger = logging.getLogger(__name__)

class TestFacturationProd(unittest.TestCase):

    def setUp(self):
        files = {
            39: "./fac_files/Folios/BoletaElectronicaProd/1001-1501/FoliosSII763237523910012021141633.xml"
        }

        with open(files[39], "rb") as archivo:
            file = archivo.read()
            self.folio39 = base64.b64encode(file).decode()


    def test_1_send39(self):
        with open("../facturacion_electronica/fac_files/ejemplos/39prod1002.json","r") as archivo:
            arch = archivo.read()
            documents = json.loads(arch)
            for document in documents['Documento']:
                document['caf_file'] = [self.folio39]
            response = fe.timbrar_y_enviar(documents)
            self.assertEqual(response['estado_sii'],'REC')


    def test_2_consult39(self):
        with open("../facturacion_electronica/fac_files/ejemplos/dte_39_ejemplo_consulta.json","r") as archivo:
            arch = archivo.read()
            documents = json.loads(arch)
            resultado = fe.consulta_estado_documento(documents)
            self.assertEqual(resultado['T39F1004']['status'],'Proceso')


if __name__ == '__main__':
    unittest.main()
