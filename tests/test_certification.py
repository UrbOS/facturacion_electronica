# -*- coding: utf-8 -*-
from lxml import etree
import unittest
import logging
from io import open
import json
from facturacion_electronica import facturacion_electronica as fe
import base64
_logger = logging.getLogger(__name__)

class TestFacturation(unittest.TestCase):

    def setUp(self):
        files = {
            33: "./fac_files/Folios/FacturaElectronica/197-209/FoliosSII763237523319720201121333.xml",
            43: "./fac_files/Folios/LiquidacionFactura/24-26/FoliosSII763237524324202011201725.xml",
            46: "./fac_files/Folios/FacturaCompra/61-120/FoliosSII763237524661202011191529.xml",
            39: "./fac_files/Folios/BoletaElectronica/101-150/FoliosSII7632375239101202010291227.xml"
        }
        with open(files[33], "rb") as archivo:
            file = archivo.read()
            self.folio33 = base64.b64encode(file).decode()

        with open(files[46], "rb") as archivo:
            file = archivo.read()
            self.folio46 = base64.b64encode(file).decode()

        with open(files[43], "rb") as archivo:
            file = archivo.read()
            self.folio43 = base64.b64encode(file).decode()

        with open(files[39], "rb") as archivo:
            file = archivo.read()
            self.folio39 = base64.b64encode(file).decode()

#    def test_caf33(self):
#        self.assertEqual(type(self.folio33),str)

    # def test_caf46(self):
    #     self.assertEqual(type(self.folio46),str)

    # def test_caf43(self):
    #     self.assertEqual(type(self.folio43),str)

#    def test_caf39(self):
#        self.assertEqual(type(self.folio39),str)

#    def test_send33(self):
#        with open("./fac_files/ejemplos/dte_33_ejemplo.json","r") as archivo:
#            documents = json.loads(archivo.read())
#            for document in documents['Documento']:
#                document['caf_file'] = [self.folio33]
#            response = fe.timbrar_y_enviar(documents)
#            _logger.warning(response['sii_xml_response'])
#            xml = etree.fromstring(response['sii_xml_response'])
            # _logger.warning(xml.tostring(root))


            # self.assertEqual(archivo,archivo)

#    def test_send46(self):
#        with open("./fac_files/ejemplos/dte_46_ejemplo.json","r") as archivo:
#            documents = json.loads(archivo.read())
#            for document in documents['Documento']:
#                document['caf_file'] = [self.folio46]
    
#            response = fe.timbrar_y_enviar(documents)
#            _logger.warning(response['sii_xml_response'])
            # xml = etree.fromstring(response['sii_xml_response'])
            # _logger.warning(xml.tostring(root))


            # self.assertEqual(archivo,archivo)

    # def test_send43(self):
    #     with open("./fac_files/ejemplos/dte_43_ejemplo.json","r") as archivo:
    #         documents = json.loads(archivo.read())
    #         for document in documents['Documento']:
    #             document['caf_file'] = [self.folio43]
    #
    #         response = fe.timbrar_y_enviar(documents)
    #         _logger.warning(response['sii_xml_response'])
            # xml = etree.fromstring(response['sii_xml_response'])
            # _logger.warning(xml.tostring(root))


            # self.assertEqual(archivo,archivo)

    def test_1_send39(self):
        with open("./fac_files/ejemplos/dte_39_ejemplo.json","r") as archivo:
            arch = archivo.read()
            documents = json.loads(arch)
            for document in documents['Documento']:
                document['caf_file'] = [self.folio39]
            response = fe.timbrar_y_enviar(documents)
            self.assertEqual(response['estado_sii'],'REC')           
    

    def test_2_consult39(self):
        with open("../facturacion_electronica/fac_files/ejemplos/dte_39_ejemplo_consulta.json","r") as archivo:
            arch = archivo.read()
            documents = json.loads(arch)
            resultado = fe.consulta_estado_documento(documents)
            self.assertEqual(resultado['T39F1004']['status'],'Proceso')



if __name__ == '__main__':
    unittest.main()
